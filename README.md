## Input Templates
```html
* <input type="text">	
* <input type="radio">
* <input type="checkbox">	
* <input type="submit">	
* <input type="button">
* <input type="range">
* <input type="week">
* <input type="reset">
* <input type="month">
* <input type="color">
* <input type="hidden">
* <input type="file">
* <input type="date">
* <input type="datetime">
* <input type="datetime-local">
* <input type="email">
* <input type="password">
* <input type="image">
```
